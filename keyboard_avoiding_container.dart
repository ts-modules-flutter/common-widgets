import 'package:flutter/material.dart';

class KeyboardAvoidingContainer extends StatelessWidget {
  final Widget child;
  const KeyboardAvoidingContainer({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: child,
      onTap: () {
        FocusScope.of(context).unfocus();
      },
    );
  }
}