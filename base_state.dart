import 'package:flutter/material.dart';

final RouteObserver<Route> routeObserver = RouteObserver<Route>();

// describe next in main:
// MaterialApp(
//       navigatorObservers: [routeObserver],

///Track route appears (by methods didPush, didPopNext).
///
///BaseState gives an opportunity to track app lifecycle state changes
abstract class BaseState<T extends StatefulWidget> extends State<T>
    with WidgetsBindingObserver, RouteAware {
  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    appLifecycleStateChanged(state);
  }

  appLifecycleStateChanged(AppLifecycleState state);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void didPush() {
    // Route was pushed onto navigator and is now topmost route.
    didAppear();
  }

  @override
  void didPopNext() {
    // Covering route was popped off the navigator.
    didAppear();
  }

  didAppear();
}
